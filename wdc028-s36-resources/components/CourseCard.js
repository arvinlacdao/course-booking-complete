import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({ courseProp }) {
    const {name, description, price} = courseProp;

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(10);
    const [isOpen, setIsOpen] = useState(true);

    function enroll(courseId){
        fetch('http://localhost:4000/api/users/enroll', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
        })
    }

    useEffect(() => {
        if(seats === 0){
            setIsOpen(false);
        }
    }, [seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>
                    <span className="subtitle">Description:</span>
                    <br />
                    {description}
                    <br />
                    <span className="subtitle">Price: </span>
                    PHP {price}
                </Card.Text>

                {isOpen ? 
                    <Button 
                        className="bg-primary" 
                        onClick={() => {enroll(courseProp._id)}}
                    >
                        Enroll
                    </Button>
                :
                    <Button className="bg-danger" disabled>Not Available</Button>
                }
                
            </Card.Body>
        </Card>
    )
}

CourseCard.propTypes = {
    // shape() - used to check that the prop conforms 
    // to a specific "shape"
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}